#!/usr/bin/env bash
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Helper script to start the winter report webservice manually for output monitoring.
# This file is not in use by the toolchain and makes the service more verbose.

set -e
set -x

if [[ -z ${AWSOME_BASE} ]]; then
        export AWSOME_BASE=/opt/awsome/code
        echo "install_webapps.sh set AWSOME_BASE to $AWSOME_BASE"
fi
if [[ -z ${AWSOME_VENVS} ]]; then
        export AWSOME_VENVS=/opt/awsome/venvs
        echo "install_webapps.sh set AWSOME_VENVS to $AWSOME_VENVS"
fi

export AWSOME_WR_TIMERS=True # debug timers when started through here

$AWSOME_VENVS/winterreport/bin/python -m gunicorn winter_report:server --reload # reload on file change

unset AWSOME_WR_TIMERS
