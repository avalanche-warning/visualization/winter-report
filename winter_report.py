################################################################################
# Copyright 2023 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# Synopsis: python3 -m gunicorn --workers=4 winter_report:server -t 300

import report_plotting
from AWSlider import AWSlider, AWSliderFigure
import awio
from awgen import awtools
import awset
from awsmet import SMETParser
import dash
import dash_bootstrap_components as dbc
import datetime
import glob
import os
import pandas as pd
import plotly.express as px
import time
import urllib

# To add a plot:
# 1. Write your plotting function accepting start- and end-dates.
# 2. Assign it a new index AWSlider.idx_* as global.
# 3. Add a viewpoint store for it in the app's layout.
# 4. Create a placeholder fig and init an AWSliderFigure with it.
# 5. Add a callback for your index and plot type.
# 6. Plot the real figure from there.
# 7. Check that the callback to restore viewpoints correctly receives the index.

_domains = awtools.list_aw_domains()
_dd_domains = []
_timers = os.environ.get("AWSOME_WR_TIMERS", False)

# We init the domains globally at startup to fill the domain dropdown:
for dom in _domains:
    if not awset.is_enabled("report", dom):
        continue
    pois = awtools.get_pois(dom)
    if pois:
        _dd_domains.append( {"label": dom.capitalize(), "value": dom})

_graph_conf = {
    "displaylogo": False,
}

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.YETI])
app.title = "AWSOME Winter Report"

# WSGI server entry point.
# Watch output via journalctl -u winter_report -f
server = app.server # e. g. gunicorn winter_report:server

##########################################
#               APP LAYOUT               #
##########################################

_lightblue = "#19AAFF"

if not _dd_domains:
    app.layout = dash.html.Div("No domain has winter reports enabled, or there are no defined points of interest.")
else:
    app.layout = dash.html.Div([
        dash.dcc.Location(id="url", refresh=False), # keep in app
        # hidden internal variables:
        dash.html.Div(id="var-domain", style={"display": "none"}, children="none"),
        dash.html.Div(id="var-poi", style={"display": "none"}, children="none"),
        dash.html.H2(dash.html.B(id="header_title", style={"color": _lightblue})),
        # domain and POI selection:
        dash.html.P(children=[
            dash.html.Div([
                dbc.Row([
                    dbc.Col( # vertically align labels and dropdowns
                        dash.html.B("Domain:", style={"display": "inline-block"}),
                        align="center", width="auto",
                    ),
                    dbc.Col( # the domain dropdown menu
                        dash.dcc.Dropdown(
                            _dd_domains,
                            id="dropdown-domain",
                            value=_dd_domains[0]["value"],
                            style={"display": "inline-block", "minWidth": "200px"}
                        ),
                        align="center", width="auto",
                    ),
                    dbc.Col(
                        dash.html.B("POI:", style={"display": "inline-block"}),
                        align="center", width="auto",
                    ),
                    dbc.Col( # the POI dropdown menu
                        dash.dcc.Dropdown(
                            id="dropdown-pois",
                            style={"display": "inline-block", "minWidth": "200px"}
                        ),
                        align="center", width="auto",
                    ),
                ]),
            ], className="dash-bootstrap"), # style according to rest of layout
        ]),
        # the tab menu at the top:
        dbc.Tabs(id="header-tabs", children=[
            dbc.Tab(label="Overview", tab_id="tab-overview"),
            dbc.Tab(label="Weather", tab_id="tab-weather"),
            dbc.Tab(label="Snow", tab_id="tab-snow"),
            dbc.Tab(label="Temperature", tab_id="tab-temperature"),
            dbc.Tab(label="Wind", tab_id="tab-wind"),
            dbc.Tab(label="Precipitation", tab_id="tab-precipitation"),
            dbc.Tab(label="Terrain", tab_id="tab-terrain"),
            dbc.Tab(label="Data Atlas", tab_id="tab-atlas"),
        ]),
        # main content:
        dash.html.Div(id="tabs-content"),
        # footnote:
        dash.html.Div([
            dash.html.Footer(id="footer-generated", style={"color": "gray"}),
            dash.html.A("Powered by AWSOME", href="https://gitlab.com/avalanche-warning", target='_blank')
        ]),
        dash.dcc.Store(id={"type": "store-time-viewpoint", "index": AWSlider.idx_SNOWHEIGHT}),
        dash.dcc.Store(id={"type": "store-range-viewpoint", "index": AWSlider.idx_PRECIPITATION}),
        dash.dcc.Store(id={"type": "store-range-viewpoint", "index": AWSlider.idx_SNOWDIFF}),
        dash.dcc.Store(id={"type": "store-range-viewpoint", "index": AWSlider.idx_TEMPERATURE}),
        dash.dcc.Store(id={"type": "store-time-viewpoint", "index": AWSlider.idx_ALBEDO}),
    ], style={"margin": "20px"})

#########################################
#               CALLBACKS               #
#########################################

@dash.callback(
    dash.Output("var-domain", "children", allow_duplicate=True),
    dash.Input("dropdown-domain", "value"),
    prevent_initial_call=True
)
def domain_select(domain):
    """Callback when the user selects a domain.

    Sets the internal "domain" variable for lookup.
    """
    return domain

@dash.callback(
    dash.Output("var-poi", "children", allow_duplicate=True),
    dash.Input("dropdown-pois", "value"),
    prevent_initial_call=True
)
def poi_select(poi):
    """Callback when the user selects a POI.

    Sets the internal "poi" variable for lookup.
    """
    return poi

@dash.callback(
    dash.Output("dropdown-pois", "options"),
    dash.Input("var-domain", "children"),
    dash.State("var-poi", "children"),
)
def domain_changed(domain, poi):
    """Callback when the internal "domain" variable has been changed.

    Lists the available POIs in their dropdown menu.
    """
    pois = awtools.get_pois(domain)
    if not pois:
        raise dash.exceptions.PreventUpdate
    dd_pois = [] # list that can be passed as dropdown menu items
    for poi in pois:
        dd_pois.append( {"label": poi["locationName"].capitalize(),
            "value": poi["locationName"]} )

    active_idx_ = 0 # check which item to select on init
    for ii in range(len(dd_pois)):
        if dd_pois[ii]["label"] == poi:
            active_idx_ = ii
            break
    return dd_pois

@app.callback(
    dash.Output("header_title", "children"),
    dash.Output("footer-generated", "children"),
    dash.Output("tabs-content", "children"),
    dash.Output("dropdown-pois", "value"),
    dash.Input("header-tabs", "active_tab"),
    dash.State("var-domain", "children"),
    dash.Input("var-poi", "children"),
)
def poi_changed(tab, domain, poi_name):
    """Callback when the internal "poi" variable has been changed.

    When a user selects a POI we start calculating, this is the main
    modeling entry point.
    Sets the page header title, the footnote, the main content,
    the correct POI dropdown item, the visibility of the wind rose
    time control, the wind rose slider's min-max-value-marks properties.
    """
    poi = awtools.select_full_poi(domain, poi_name)
    wrf_file = _get_last_wrffile(domain)

    if tab == "tab-overview":
        graph = content_overview(domain, poi)
    elif tab == "tab-weather":
        graph = content_weather(domain, poi, wrf_file)
    elif tab == "tab-snow":
        graph = content_snow(domain, poi, wrf_file)
    elif tab == "tab-temperature":
        graph = content_temperature(domain, poi, wrf_file)
    elif tab == "tab-wind":
        graph = content_wind(domain, poi, wrf_file)
    elif tab == "tab-precipitation":
        graph = content_precipitation(domain, poi, wrf_file)
    elif tab == "tab-terrain":
        graph = content_terrain(domain, poi, wrf_file)
    elif tab == "tab-atlas":
        graph = content_atlas(domain, poi, wrf_file)
    else:
        graph = dash.html.Span(dash.html.B(
            f'Internal error: unknown tab name "{tab}".'), style={"color": "red"})

    today = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    title = f'Winter report for domain "{domain.capitalize()}", POI "{poi["locationName"].capitalize()}"'
    subtitle = f"Generated on {today}"
    return (
        title,
        subtitle,
        graph,
        poi["locationName"],
    )

@app.callback(
    dash.Output({"type": "rangeslider-graph", "index": AWSlider.idx_WINDROSE}, "figure"),
    dash.Input({"type": "rangeslider-alldates", "index": AWSlider.idx_WINDROSE}, "value"),
    dash.State("var-domain", "children"),
    dash.State("var-poi", "children"),
)
def windrose_time_changed(value, domain, poi_name):
    """Callback for when the wind rose slider range has been set."""
    poi = awtools.select_full_poi(domain, poi_name)
    if not poi:
        return report_plotting._emptyfig("No POIs in domain.")

     # the selected time range is in the value list:
    sdate = pd.to_datetime(value[0], unit="s", origin="unix")
    edate = pd.to_datetime(value[1], unit="s", origin="unix")

    stime = time.time_ns()
    figrose = report_plotting.wind_rose(domain, poi, sdate=sdate, edate=edate) # wind speed frequency plot
    if _timers: print(f"[T] figrose: {awio.iso_timediff(stime, True)}")
    return figrose # here we update the figure directly

@app.callback(
    dash.Output({"type": "time-label-start", "index": dash.MATCH}, "children"),
    dash.Output({"type": "time-label-end", "index": dash.MATCH}, "children"),
    dash.State("var-domain", "children"),
    dash.Input({"type": "timeslider", "index": dash.MATCH}, "drag_value"),
)
def _update_timeslider_labels(domain, drag_value):
    """Callback when the user drags a range slider.

    This is before the user lets go of the slider and sets the value,
    where the value stands for unix seconds.
    Sets the start and end date labels.
    """
    if not drag_value:
        raise dash.exceptions.PreventUpdate
    
    wrf_file = _get_last_wrffile(domain)
    dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(dates)
    try:
        sdate = pd.to_datetime(dates_stepped[drag_value[0]][0])
        edate = pd.to_datetime(dates_stepped[drag_value[1]][0])
    except TypeError:
        sdate = ""
        edate = pd.to_datetime(dates_stepped[drag_value][0])
    return sdate, edate

@app.callback(
    dash.Output({"type": "range-label-alldates-start", "index": dash.MATCH}, "children"),
    dash.Output({"type": "range-label-alldates-end", "index": dash.MATCH}, "children"),
    dash.Input({"type": "rangeslider-alldates", "index": dash.MATCH}, "drag_value"),
)
def _update_rangeslider_alldates_labels(drag_value):
    """Callback when the user drags a range slider.

    This is before the user lets go of the slider and sets the value,
    where the value stands for unix seconds.
    Sets the start and end date labels.
    """
    if not drag_value:
        raise dash.exceptions.PreventUpdate
    sdate = pd.to_datetime(drag_value[0], unit="s", origin="unix")
    edate = pd.to_datetime(drag_value[1], unit="s", origin="unix")
    return sdate, edate

@app.callback(
    dash.Output({"type": "range-label-selected-start", "index": dash.MATCH}, "children"),
    dash.Output({"type": "range-label-selected-end", "index": dash.MATCH}, "children"),
    dash.State("var-domain", "children"),
    dash.Input({"type": "rangeslider-selected", "index": dash.MATCH}, "drag_value"),
)
def _update_rangeslider_selected_labels(domain, drag_value):
    """Callback when the user drags a range slider.

    This is before the user lets go of the slider and sets the value,
    where the value is yet to be mapped.
    Sets the start and end date labels.
    """
    if not drag_value:
        raise dash.exceptions.PreventUpdate
    
    wrf_file = _get_last_wrffile(domain)
    dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(dates)
    try:
        sdate = pd.to_datetime(dates_stepped[drag_value[0]][0])
        edate = pd.to_datetime(dates_stepped[drag_value[1]][0])
    except TypeError:
        sdate = ""
        edate = pd.to_datetime(dates_stepped[drag_value][0])
    return sdate, edate

@app.callback(
    dash.Output({"type": "timeslider-graph", "index": AWSlider.idx_SNOWHEIGHT}, "figure"),
    dash.State("var-domain", "children"),
    dash.State("var-poi", "children"),
    dash.Input({"type": "timeslider", "index": AWSlider.idx_SNOWHEIGHT}, "value"),
    dash.State({"type": "store-time-viewpoint", "index": AWSlider.idx_SNOWHEIGHT}, "data")
)
def snowheight_time_changed(domain, poi_name, value, stored_view):
    """Callback for when user releases snow height map slider."""
    poi = awtools.select_full_poi(domain, poi_name)
    wrf_file = _get_last_wrffile(domain)
    dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(dates)
    fighs = report_plotting.single_raster_chart(domain, poi, "SNOWH", wrf_file,
        bounds=[0, None], timeidx=dates_stepped[value][1], title="Physical snow depth from WRF forecast (m):")
    if stored_view:
        # We have a hook for when the user changes the viewpoint, where it is saved to
        # a Store. Here we retrieve this but have to de-serialize tuples manually.
        view = {}
        try:
            view["xaxis.range"] = (stored_view["xaxis.range[0]"], stored_view["xaxis.range[1]"])
        except KeyError:
            pass # no harm if the setting just does not exist
        try:
            view["yaxis.range"] = (stored_view["yaxis.range[0]"], stored_view["yaxis.range[1]"])
        except KeyError:
            pass
        fighs.update_layout(view)
    return fighs

@app.callback(
    dash.Output({"type": "timeslider-graph", "index": AWSlider.idx_ALBEDO}, "figure"),
    dash.State("var-domain", "children"),
    dash.State("var-poi", "children"),
    dash.Input({"type": "timeslider", "index": AWSlider.idx_ALBEDO}, "value"),
    dash.State({"type": "store-time-viewpoint", "index": AWSlider.idx_ALBEDO}, "data")
)
def albedo_time_changed(domain, poi_name, value, stored_view):
    """Callback for when user releases snow height map slider."""
    wrf_file = _get_last_wrffile(domain)
    poi = awtools.select_full_poi(domain, poi_name)
    bounds = [[0, 1]] * 3 + [[0, 15]]
    params = ["ALBEDO", "ALBBCK", "SNOALB", "LU_INDEX"]
    titles = ("Albedo", "Background Albedo", "Annual max snow albedo in fraction", "Landuse index")
    figalb = report_plotting.subplots_map(poi, wrf_file, params, time_step=value, bounds=bounds, titles=titles)
    if stored_view:
        view = {}
        try:
            view["xaxis.range"] = (stored_view["xaxis.range[0]"], stored_view["xaxis.range[1]"])
            view["xaxis2.range"] = (stored_view["xaxis2.range[0]"], stored_view["xaxis2.range[1]"])
            view["xaxis3.range"] = (stored_view["xaxis3.range[0]"], stored_view["xaxis3.range[1]"])
            view["xaxis4.range"] = (stored_view["xaxis4.range[0]"], stored_view["xaxis4.range[1]"])
        except KeyError:
            pass
        try:
            view["yaxis.range"] = (stored_view["yaxis.range[0]"], stored_view["yaxis.range[1]"])
            view["yaxis2.range"] = (stored_view["yaxis2.range[0]"], stored_view["yaxis.range[1]"])
            view["yaxis3.range"] = (stored_view["yaxis3.range[0]"], stored_view["yaxis.range[1]"])
            view["yaxis4.range"] = (stored_view["yaxis4.range[0]"], stored_view["yaxis.range[1]"])
        except KeyError:
            pass
        figalb.update_layout(view)
    return figalb

@app.callback(
    dash.Output({"type": "rangeslider-graph", "index": AWSlider.idx_PRECIPITATION}, "figure"),
    dash.State("var-domain", "children"),
    dash.State("var-poi", "children"),
    dash.Input({"type": "rangeslider-selected", "index": AWSlider.idx_PRECIPITATION}, "value"),
    dash.Input({"type": "radio-precipitation", "index": AWSlider.idx_PRECIPITATION}, "value"), # also a pattern to "exist" on startup
    dash.State({"type": "store-range-viewpoint", "index": AWSlider.idx_PRECIPITATION}, "data")
)
def precip_time_changed(domain, poi_name, value, param, stored_view):
    wrf_file = _get_last_wrffile(domain)
    poi = awtools.select_full_poi(domain, poi_name)
    dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(dates)
    sdate = pd.to_datetime(dates_stepped[value[0]][0])
    edate = pd.to_datetime(dates_stepped[value[1]][0])

    stime = time.time_ns()
    figpre = report_plotting.param_heatmap(poi, wrf_file, param, sdate=sdate, edate=edate)
    if _timers: print(f"[T] figpre: {awio.iso_timediff(stime, True)}")
    if stored_view:
        stored_view.pop("mapbox._derived", None) # can't pass that one back
        figpre.update_layout(stored_view)
    return figpre

@app.callback(
    dash.Output({"type": "rangeslider-graph", "index": AWSlider.idx_SNOWDIFF}, "figure"),
    dash.State("var-domain", "children"),
    dash.State("var-poi", "children"),
    dash.Input({"type": "rangeslider-selected", "index": AWSlider.idx_SNOWDIFF}, "value"),
    dash.State({"type": "store-range-viewpoint", "index": AWSlider.idx_SNOWDIFF}, "data")
)
def heatmap_time_changed(domain, poi_name, value, stored_view):
    wrf_file = _get_last_wrffile(domain)
    poi = awtools.select_full_poi(domain, poi_name)
    dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(dates)
    sdate = pd.to_datetime(dates_stepped[value[0]][0])
    edate = pd.to_datetime(dates_stepped[value[1]][0])

    stime = time.time_ns()
    figparam = report_plotting.param_heatmap(poi, wrf_file, param="SNOWH",
        sdate=sdate, edate=edate, accumulated=False, colormap="RdBu", zero_trans=True)
    if _timers: print(f"[T] figparam: {awio.iso_timediff(stime, True)}")
    if stored_view:
        stored_view.pop("mapbox._derived", None) # can't pass that one back
        figparam.update_layout(stored_view)
    return figparam

@app.callback(
    dash.Output({"type": "timeslider-graph", "index": AWSlider.idx_TEMPERATURE}, "figure"),
    dash.State("var-domain", "children"),
    dash.State("var-poi", "children"),
    dash.Input({"type": "timeslider", "index": AWSlider.idx_TEMPERATURE}, "value"),
    dash.State({"type": "store-range-viewpoint", "index": AWSlider.idx_TEMPERATURE}, "data")
)
def temperature_time_changed(domain, poi_name, value, stored_view):
    wrf_file = _get_last_wrffile(domain)
    poi = awtools.select_full_poi(domain, poi_name)
    dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(dates)
    try:
        sdate = pd.to_datetime(dates_stepped[value[0]][0])
        edate = pd.to_datetime(dates_stepped[value[1]][0])
    except TypeError:
        sdate = None
        edate = pd.to_datetime(dates_stepped[value][0])

    stime = time.time_ns()

    figparam = report_plotting.param_heatmap(poi, wrf_file, param="T2", add=-273.15, units="°C",
        sdate=sdate, edate=edate, accumulated=False, single_value=True, colormap="Jet")
    if _timers: print(f"[T] figparam: {awio.iso_timediff(stime, True)}")
    if stored_view:
        stored_view.pop("mapbox._derived", None) # can't pass that one back
        figparam.update_layout(stored_view)
    return figparam

@app.callback(
    dash.Output({"type": "store-time-viewpoint", "index": dash.MATCH}, "data"),
    dash.Input({"type": "timeslider-graph", "index": dash.MATCH}, "relayoutData"),
    dash.State({"type": "store-time-viewpoint", "index": dash.MATCH}, "data"),
)
def _update_timeslidergraph_viewpoint(layout_data, saved_viewpoint):
    """Callback for when the snowheight plot's viewpoint has been changed.

    This function saves the viewpoint to be able to apply it when changing
    the slider value (since there we re-draw the whole plot).
    """
    if saved_viewpoint and layout_data:
        # The relayout trigger is incremental, meaning if we first change the
        # x range, then the y range we need to keep track of both changes.
        # For this we merge the stored viewpoint with the new one.
        # However, we must remove range settings if "autorange" has been the
        # last command else it may not take precedence.
        if "xaxis.autorange" in layout_data:
            saved_viewpoint.pop("xaxis.range[0]", None)
            saved_viewpoint.pop("xaxis.range[1]", None)
        if "yaxis.autorange" in layout_data:
            saved_viewpoint.pop("yaxis.range[0]", None)
            saved_viewpoint.pop("yaxis.range[1]", None)
        return {**saved_viewpoint, **layout_data} # merge dicts
    else:
        if not layout_data:
            raise dash.exceptions.PreventUpdate
        return layout_data

@app.callback(
    dash.Output({"type": "store-range-viewpoint", "index": dash.MATCH}, "data"),
    dash.Input({"type": "rangeslider-graph", "index": dash.MATCH}, "relayoutData"),
    dash.State({"type": "store-range-viewpoint", "index": dash.MATCH}, "data"),
)
def _update_rangeslidergraph_viewpoint(layout_data, saved_viewpoint):
    """Callback for when the snowheight plot's viewpoint has been changed.

    This function saves the viewpoint to be able to apply it when changing
    the slider value (since there we re-draw the whole plot).
    """
    if layout_data and saved_viewpoint:
        return {**saved_viewpoint, **layout_data} # merge dicts
    else:
        return layout_data

@app.callback(
    dash.Output("var-domain", "children"),
    dash.Output("var-poi", "children"),
    dash.Input("url", "href")
)
def url_changed(href):
    """Callback for when the URL has changed.

    This allows for query parameters when opening the web app.
    These are: https://winter.avalanche.report?domain=<domain>&poi=<poi>
    Sets the domain and poi internal variables.
    """
    if not href:
        raise dash.exceptions.PreventUpdate
    parsed_url = urllib.parse.urlparse(href)
    params = urllib.parse.parse_qs(parsed_url.query)
    domain = params.get("domain", [""])[0]
    poi_name = params.get("poi", [""])[0]
    if not domain:
        domain = _dd_domains[0]["value"]
    return domain, poi_name

################################################
#               CONTENT CREATION               #
################################################

def content_overview(domain: str, poi: dict):
    data_path = os.path.join(awio.get_scriptpath(__file__),
        "station_data", f"{domain.lower()}-{poi['locationName'].lower()}")
    data_files = os.listdir(data_path)
    data_files.sort() # wind station before weather station
    smet = SMETParser(os.path.join(data_path, data_files[0]))
    marker_df = smet.df() # deep copy
    if marker_df is None:
        graph = dash.html.Div(id="overview-figs", children=[
            dash.dcc.Graph(figure=report_plotting._emptyfig("Error fetching ground station data.")),
        ])
        return graph

    wrf_file = _get_last_wrffile(domain)
    stime = time.time_ns()
    figns = report_plotting.snow_sums(domain, poi, wrf_file) # new snow amounts
    if _timers: print(f"[T] figns: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    figover = report_plotting.overview_plot(domain, poi) # overview plot of important meteo parameters:
    if _timers: print(f"[T] figover: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    figsol = report_plotting.solar_plot(domain, poi)
    if _timers: print(f"[T] figsol: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    figmaxvw = report_plotting.wind_barbs(domain, poi)
    if _timers: print(f"[T] figmaxvw: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    figrose = report_plotting._emptyfig("Calculating...")
    if _timers: print(f"[T] figrose: {awio.iso_timediff(stime, True)}")

    slifig = AWSliderFigure(figure=figrose, marker_df=marker_df, index=AWSlider.idx_WINDROSE, graph_conf=_graph_conf)
    graph = dash.html.Div(id="overview-figs", children=[
        dash.dcc.Graph(figure=figover, style={"height": "60vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figns, style={"height": "60vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figsol, style={"height": "50vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figmaxvw, style={"height": "50vh"}, config=_graph_conf),
        slifig,
    ])
    return graph

def content_weather(domain: str, poi: dict, wrf_file: str):
    figcld = report_plotting.clouds_plot(domain, poi, wrf_file, _timers=_timers) # cloud fraction at altitude
    graph = dash.dcc.Graph(figure=figcld, style={"height": "90vh"}, config=_graph_conf)
    return graph

def content_snow(domain: str, poi: dict, wrf_file: str):
    fighs = report_plotting._emptyfig("Calculating...") # filled instantly on time slider set
    fighsts = report_plotting.snow_height_timeseries(poi, wrf_file)
    fighsdiff = report_plotting._emptyfig("Calculating...")
    figs3d = report_plotting.snowmap_3d(poi, wrf_file)
    figtsno = report_plotting.snow_temp_plot(poi, wrf_file)

    dates = report_plotting._wrf_dates(wrf_file, _timers=_timers)
    dates_stepped = awio.choose_display_dates(dates)
    
    sl_fighs = AWSliderFigure(figure=fighs, dates_stepped=dates_stepped, single_value=True, index=AWSlider.idx_SNOWHEIGHT, graph_conf=_graph_conf)
    sl_fighsdiff = AWSliderFigure(figure=fighsdiff, dates_stepped=dates_stepped, index=AWSlider.idx_SNOWDIFF, graph_conf=_graph_conf)

    graph = dash.html.Div(id="snow-figs", children=[
        dash.dcc.Graph(figure=fighsts, style={"height": "60vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figtsno, style={"height": "60vh"}, config=_graph_conf),
        sl_fighs,
        sl_fighsdiff,
        dash.dcc.Graph(figure=figs3d, style={"height": "90vh"}, config=_graph_conf),
    ])
    return graph

def content_temperature(domain: str, poi: dict, wrf_file: str):
    dates = report_plotting._wrf_dates(wrf_file, _timers=_timers)
    dates_stepped = awio.choose_display_dates(dates)
    dates_short=[pd.to_datetime(dt[0]).strftime("%m-%d %H-%M") for dt in dates_stepped]

    figta = report_plotting._emptyfig("Calculating...")
    figtalvl = report_plotting.temperature_levels(wrf_file, _timers=_timers)
    fig0d = report_plotting.zero_deg_line(poi, wrf_file, domain, _timers=_timers)
    figts = report_plotting.single_raster_chart(domain, poi, "TSK", wrf_file)
    figtc = report_plotting.temperature_crosssection(domain, poi, wrf_file)

    sl_figta = AWSliderFigure(figure=figta, dates_stepped=dates_stepped, single_value=True, index=AWSlider.idx_TEMPERATURE, graph_conf=_graph_conf)
    graph = dash.html.Div(id="temperature-figs", children=[
        sl_figta,
        dash.dcc.Graph(figure=figtalvl, style={"height": "90vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=fig0d, style={"height": "90vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figts, style={"height": "90vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figtc, style={"height": "90vh"}, config=_graph_conf),
    ])
    return graph

def content_wind(domain: str, poi: dict, wrf_file: str):
    figw = report_plotting.windmap_uvwz(wrf_file)
    graph = dash.dcc.Graph(figure=figw, style={"height": "90vh"}, config=_graph_conf)
    return graph

def content_precipitation(domain: str, poi: dict, wrf_file: str):
    fighsts, annotation = report_plotting.precip_timeseries(poi, wrf_file)
    wrf_dates = report_plotting._wrf_dates(wrf_file)
    dates_stepped = awio.choose_display_dates(wrf_dates)

    figpre = report_plotting._emptyfig("Calculating...")
    slider_precip = AWSlider(dates_stepped=dates_stepped, index=AWSlider.idx_PRECIPITATION)

    graph = dash.html.Div(id="precip-figs", children=[
        dash.html.Div(children=[
            dash.dcc.Graph(figure=fighsts, style={"height": "60vh"}, config=_graph_conf),
            dash.dcc.Markdown(annotation, style={"width": "75%", "margin": "auto", "fontSize": "smaller"}),
            dash.html.Div(children=[
                dbc.Row([
                    dbc.Col(
                        dbc.RadioItems(
                            id={"type": "radio-precipitation", "index": AWSlider.idx_PRECIPITATION},
                            class_name="btn-group-vertical",
                            inputClassName="btn-check",
                            labelClassName="btn btn-outline-primary",
                            labelCheckedClassName="active",
                            options=[
                                {"label": "Snow", "value": "SNOWNC"},
                                {"label": "Rain", "value": "RAINNC"},
                                {"label": "Graupel", "value": "GRAUPELNC"},
                                {"label": "Hail", "value": "HAILNC"},
                            ],
                            value="SNOWNC",
                        ),
                        width=1,
                    ),
                    dbc.Col(
                        dash.dcc.Graph(
                            figure=figpre,
                            style={"height": "80vh"},
                            config=_graph_conf,
                            id={"type": "rangeslider-graph", "index": AWSlider.idx_PRECIPITATION}
                        ),
                    )
                ]),
                slider_precip,
            ], style={"margin-bottom": "30px"}), # needs some bottom space for the markers
        ]),
    ])
    return graph

def content_terrain(domain: str, poi: dict, wrf_file: str):
    figalb = report_plotting._emptyfig("Calculating...")
    dates = report_plotting._wrf_dates(wrf_file, _timers=_timers)
    dates_stepped = awio.choose_display_dates(dates)
    sl_figalb = AWSliderFigure(figalb, dates_stepped=dates_stepped,
        single_value=True, index=AWSlider.idx_ALBEDO, graph_conf=_graph_conf)
    graph = dash.html.Div(id="terrain-figs", children=[
        sl_figalb,
    ])
    return graph

def content_atlas(domain: str, poi: dict, wrf_file: str):
    figsstat = report_plotting.station_plots(domain, poi)
    fignwp = report_plotting.nwp_plot(poi, wrf_file, param_set="all")
    graph = dash.html.Div(id="snow-figs", children=[
        dash.dcc.Graph(figure=figsstat[1], style={"height": "60vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=figsstat[0], style={"height": "60vh"}, config=_graph_conf),
        dash.dcc.Graph(figure=fignwp, style={"height": "60vh"}, config=_graph_conf),
    ])
    return graph

################################################
#               HELPER FUNCTIONS               #
################################################

def _get_last_wrffile(domain: str) -> str:
    """Wrapper for reporting missing files"""
    wrffiles = awtools.get_last_wrffile(domain)
    if not wrffiles:
        print(f'[W] No WRF data for domain "{dom}"')
    return wrffiles
