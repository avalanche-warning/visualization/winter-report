#!/usr/bin/env python3
################################################################################
# Copyright 2023 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awio
import awgen
import awset
import datetime
import os
import subprocess

def _get_runspan(domain: str):
    sdate = awset.get(["snowpack", "mode", "season", "start"], domain)
    if awio.validate_date(sdate, "%m-%d"):
        dt = datetime.datetime.strptime(sdate, "%m-%d")
        cyear = datetime.datetime.now().year
        cmonth = datetime.datetime.now().month
        syear = cyear if cmonth >= dt.month else cyear - 1
        sdate = f"{syear}-{sdate}T00:00:00Z"
    else:
        print("[E] Unable to resolve winter report start date.")
        sys.exit()

    edate = datetime.datetime.today().isoformat()
    return sdate, edate

def dl_station_data(domain: str, poi, station: str):
    download_script_path = os.path.expanduser("~data/query/wiski/wiski_rest.py")
    os.makedirs("./station_data", exist_ok=True)
    output_dir = os.path.join(awio.get_scriptpath(__file__),
        "station_data", f"{domain.lower()}-{poi['locationName'].lower()}")
    sdate, edate = _get_runspan(domain)
    data_span = f"{sdate}/{edate}" # date format of lwdt API
    subprocess.call(["python3", download_script_path, station, data_span, output_dir])

def _process_poi(domain: str, poi):
    coords = poi["location"].split(",")
    coords = (float(coords[0]), float(coords[1]))
    stat, _ = awgen.find_closest_station(coords, "./ogd.geojson", has_field="HS")
    stat_wind, _ = awgen.find_closest_station(coords, "./ogd.geojson", station_type="wind", has_field="WG")
    id_meteo = stat["properties"]["LWD-Nummer"]
    id_wind = stat_wind["properties"]["LWD-Nummer"]
    dl_station_data(domain, poi, id_meteo)
    dl_station_data(domain, poi, id_wind)

if __name__ == "__main__":
    domains = awgen.list_aw_domains()
    out_ogd = os.path.join(awio.get_scriptpath(__file__), "ogd.geojson")
    awgen.dl_stations_list(outfile=out_ogd) # download list of available stations
    for dom in domains:
        if not awset.is_enabled("report", dom):
            continue
        pois = awgen.get_pois(dom)
        if not pois:
            continue
        for poi in pois:
            _process_poi(dom, poi)
