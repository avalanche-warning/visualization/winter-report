#!/usr/bin/env python3
################################################################################
# Copyright 2024 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import dash
import dash_bootstrap_components as dbc
import datetime
import pandas as pd

class AWSlider(dbc.Row):

    idx_WINDROSE = 1
    idx_SNOWHEIGHT = 2
    idx_SNOWDIFF = 3
    idx_PRECIPITATION = 4
    idx_TEMPERATURE = 5
    idx_ALBEDO = 6

    def __init__(self, marker_df=None, dates_stepped=None, single_value: bool=False, index: int=0, **kwargs):
        _datemarker_style = {"transform": "rotate(-45deg) translate(-30px, 0px)", "white-space": "nowrap"}

        if dates_stepped is None: # all dates
            self._slider_type = "rangeslider-alldates"
            self._start_label_type = "range-label-alldates-start"
            self._end_label_type = "range-label-alldates-end"

            weekago = datetime.datetime.now() - datetime.timedelta(days=7)
            weekago = int(weekago.timestamp()) # unix
            marker_df["unix"] = marker_df.index.astype("int64") # datetime to milliseconds
            marker_df["unix"] = marker_df["unix"].apply(lambda xx: xx // 10**9) #and to seconds
            n_marks = 20 # create this many marks on the slider
            thinning = len(marker_df) // n_marks
            marker_df = marker_df.iloc[::thinning, :]
            marker_df.index = marker_df.index.strftime('%m-%d')
            marks = dict(zip(marker_df["unix"], marker_df.index))
            for dt, label in marks.items():
                # prepare dictionary that can be passed as slider markings:
                marks[dt] = {"label": label, "style": _datemarker_style}
            self._slider = dash.dcc.RangeSlider(
                    min=marker_df["unix"].iloc[0],
                    max=marker_df["unix"].iloc[-1],
                    value=[weekago, marker_df["unix"].iloc[-1]],
                    marks=marks,
                    id={"type": self._slider_type, "index": index},
            )
        else: # selected dates only
            dates_short=[pd.to_datetime(dt[0]).strftime("%m-%d %H-%M") for dt in dates_stepped]
            marks = {}
            for ii in range(len(dates_short)):
                marks[ii] = {"label": dates_short[ii], "style": _datemarker_style}

            if single_value:
                self._slider_type = "timeslider"
                self._start_label_type = "time-label-start"
                self._end_label_type = "time-label-end"

                self._slider = dash.dcc.Slider(
                        min=0,
                        max=len(dates_stepped) - 1,
                        step=1,
                        value=len(dates_stepped) - 1,
                        marks=marks,
                        id={"type": "timeslider", "index": index},
                )
            else:
                self._slider_type = "rangeslider-selected"
                self._start_label_type = "range-label-selected-start"
                self._end_label_type = "range-label-selected-end"

                self._slider = dash.dcc.RangeSlider(
                   min=0,
                   max=len(dates_stepped) - 1,
                   step=1,
                   value=[0, len(dates_stepped) - 1],
                   marks=marks,
                   id={"type": self._slider_type, "index": index},
                ),

        children = [
            dbc.Col(
                dash.html.Plaintext(id={"type": self._start_label_type, "index": index}),
                width=2
            ),
            dbc.Col(
                self._slider,
                align="center"
            ),
            dbc.Col(
                dash.html.Plaintext(id={"type": self._end_label_type, "index": index}),
                width=2
            )
        ]
        super().__init__(children=children, **kwargs)

class AWSliderFigure(dash.html.Div):
    def __init__(self, figure, marker_df=None, dates_stepped=None, single_value: bool=False, index=0, graph_conf={}):
        self.slider = AWSlider(marker_df=marker_df, dates_stepped=dates_stepped, single_value=single_value, index=index)
        if single_value:
            self._graph_type = "timeslider-graph"
        else:
            self._graph_type = "rangeslider-graph"
        children=[
            dash.dcc.Graph(
                figure=figure,
                style={"height": "80vh"},
                config=graph_conf,
                # Dynamic ids since we must tie a callback to here (which does not
                # exist from the start). We can later match these indices and
                # perform an action on a plot via a slider when both elements
                # have the same ID.
                id={"type": self._graph_type, "index": index}
            ),
            self.slider,
        ]
        super().__init__(children=children, style={"margin-bottom": "40px"}) # needs some bottom space for the markers
