################################################################################
# Copyright 2023 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import awio
import awset
from awsmet import SMETParser
from awwrf.wrf_plot import _4d_select
from collections import OrderedDict
import datetime
from itertools import chain
import fiona
from geopy import distance
import glob
import math
from netCDF4 import Dataset
import numpy as np
import os
import pandas as pd
import plotly.io as pio
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import shapely
import time
import urllib
import warnings
import wrf
import xarray as xr

# Unactionable FutureWarning. However, if this is changed we can probably skip removing categories
warnings.simplefilter(action='ignore', category=FutureWarning)

pio.templates.default = "plotly_white"
pio.templates["plotly_white"].layout.font.size = 12
_lightblue = "#19AAFF"
_darkblue = "#00008B"
_salmonred = "#FF9999"

#################################################
#               TIME SLIDER CLASS               #
#################################################

class TimeSlider:
    """A time slider class for contour plots and heatmaps.

    Timed raster data can be added step-wise and the slider
    is updated on the fly. True image-like rasters can not
    be used this way because we are on a grid with no coordinates
    in between.
    """
    def __init__(self, fig, domain, poi, wrffile):
        self.fig = fig
        self.steps = []
        self.poi = poi

        # Our first two traces are domain shape and POI; these are
        # added in the beginning and are always kept visible (if
        # we have any POIs at all):
        if poi:
            fig_poi = _make_poi_fig(domain, poi, wrffile)
            fig_region = _make_region_fig(domain, wrffile)
            fig.add_trace(fig_poi)
            fig.add_trace(fig_region)

    def add_time_step(self, data, ptype="contour", visible=False, title=None,
            bounds: tuple=None, showscale: bool=True, colorscale=None, row=None, col=None):
        """Add a timed raster layer to the plot and update the slider."""
        # Set from outside to always have the same colors, leave at None
        # to take advantage of the full color scale and for heatmaps:
        if not bounds:
            bounds = (data.values.min(), data.values.max())

        if ptype == "contour":
            trace = go.Contour(
                        z=data,
                        visible=visible,
                        contours=dict(start=bounds[0], end=bounds[1]),
                        colorscale=colorscale,
                        line_smoothing=0.5,
                        showscale=showscale
                    )
        elif ptype == "heat":
            trace = go.Heatmap(
                    z=data,
                    visible=visible,
                    colorscale=colorscale,
                    zsmooth="best"
                )

        self.fig.add_trace(trace, row=row, col=col) # ok if row, col are None
        if self.poi: # if we have a shape and poi keep them visible:
            vispre = [True, True]
            poi_offset = 1
        else:
            vispre = []
            poi_offset = -1
        # POI visibility + all others off + last one visible:
        _visibility = vispre + [False] * (len(self.fig.data) - 3) + [True]
        step = dict(
            method="update",
            args=[ {"visible": _visibility} ]
        )
        # One slider controls all, skip all subplots:
        if (not row and not col) or (row == 1 and col == 1):
            self.steps.append(step)

        # Tell the slider to update the number of steps. If we have
        # a shape and POI we need two steps less:
        active_idx = len(self.steps) + poi_offset
        self._update(active_idx, title)

    def _update(self, active_idx, title):
        """Set steps in the slider and select the active one."""
        sliders = [{
            "active": active_idx,
            "currentvalue": {"prefix": "WRF forecast: "},
            "steps": self.steps
        }]
        
        self.fig.update_layout(
            sliders=sliders
        )
        self.fig.data[active_idx].visible = True

##########################################
#               DATA PLOTS               #
##########################################

def wind_rose(domain: str, poi: dict, sdate=None, edate=None):
    """Wind rose plot for strength in regards to direction and frequency.

    This routine looks at every data point of a time series, bins the
    measurement into aspects and strength labels and plots that as a bar
    polar chart. The colors code the strength, the radius is the frequency
    and theta is the direction bin. Hence, the stronger winds are highlighted
    by color and segment size of the circle with the "thickness" of the
    area giving the frequency, i. e. how many percent of the measurements
    fall into this category.

    Args:
        domain: The AWSOME domain (to look up weather stations).
        poi: Point of interest as geojson dict.
        sdate: Use data from this date onwards. Set None for all.
        edate: Use data before this date. Set None for all.
    Returns:
        A plotly figure.
    """

    # Data has hopefully been downloaded beforehand (we don't want the
    # user to do this in their sessions):
    data_files, data_path = _get_snow_wind_stations(domain, poi)
    if len(data_files) != 2:
        fig = _emptyfig("No wind station available.")
        return fig

    search = (file for file in data_files if file.endswith("1.smet"))
    snow_stat_path = next(search, None)
    fsmet = os.path.join(data_path, snow_stat_path)
    smet = SMETParser(fsmet) # read wind station
    # Compute the distance between the POI and the located AWS:
    stat_id, stat_name, stat_dist = _get_station_dist(smet, poi)
    df = smet.df()
    if not "VW" in df or not "DW" in df:
        fig = _emptyfig("No wind data in station.")
        return fig
    df = df.dropna(subset=["VW", "DW"])
    # remove time zone info from SMET data to be able to compare:
    # TODO: in SMETParser with correct TZ info?
    if sdate:
        df = df[df.index.tz_localize(None) >= sdate]
    if edate:
        df = df[df.index.tz_localize(None) <= edate]

    rose = df.loc[:, ["VW", "DW"]] # modify this for the plot
    rose["timestamp"] = df.index
    rose["VW"] = df["VW"].apply(lambda xx: xx * 3.6) # m/s -> km/h

    bins_dw = [-1, 22.5, 67.5, 112.5, 202.5, 247.5, 292.5, 337.5, 360]
    labels_dw = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    rose["Wind direction"] = pd.cut(rose["DW"], bins=bins_dw, labels=labels_dw) # binning by direction
    bins_vw = [0, 20, 40, 60, 100, 250]
    # https://www.avalanches.org/glossary/#wind-speed
    labels_vw = ["low (0-20 km/h)", "moderate (20-40 km/h)", "strong (40-60 km/h)",
        "very strong (60-100 km/h)", "gale, hurricane (> 100 km/h)"]
    rose["Wind speed"] = pd.cut(rose["VW"], bins=bins_vw, labels=labels_vw) # binning by strength

    # Now we have point data and two columns with direction and strength bins, the reset is
    # just for renaming:
    frequency = rose.value_counts(
        subset=["Wind direction", "Wind speed"]).reset_index().rename(columns={0: "count"})
    # compute frequency from sample size:
    frequency["frequency"] = frequency["count"].apply(lambda xx: xx / len(df.index) * 100)
    # if there is e. g. no storm we need to remove these categories before plotting:
    frequency["Wind speed"] = frequency["Wind speed"].cat.remove_unused_categories()

    # discrete colors for each strength bin:
    cmap = ["#89CFF0", "#B19CD9", "#7B68EE", "#E56717", "#FF0000"]
    fig = px.bar_polar(
        frequency,
        r="frequency",
        theta="Wind direction",
        color="Wind speed",
        category_orders={"Wind direction": labels_dw}, # North is north and opposite to South
        color_discrete_sequence=cmap,
    )
    fig.update_traces(hovertemplate = _get_windrose_hover_templ())
    fig.update_layout(
        polar={
            "angularaxis": {"tickfont": dict(size=16)},
            "radialaxis": {"ticksuffix": "%"}
        },
        title_text=f'Closest wind station "{stat_name}" ({stat_id}) is {round(stat_dist, 2)} km away:'
    )
    return fig

def overview_plot(domain: str, poi: dict):
    """Plots the most important measurements of a nearby weather station."""
    data_files, data_path = _get_snow_wind_stations(domain, poi)
    search = (file for file in data_files if not file.endswith("1.smet"))
    snow_stat_path = next(search, None)
    fsmet = os.path.join(data_path, snow_stat_path)
    smet = SMETParser(fsmet)
    stat_id, stat_name, stat_dist = _get_station_dist(smet, poi) # dist POI <-> station
    df = smet.df()

    df["TA"] = df["TA"].apply(lambda xx: xx - 273.15) # K to degC
    # px can't do two separate Y-axis so we use go:
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    hs_min_row = df[df["HS"] == df["HS"].min()].iloc[0]
    hs_max_row = df[df["HS"] == df["HS"].max()].iloc[0]

    fig.add_trace(go.Scatter(
        x=[hs_min_row.name],
        y=[hs_min_row["HS"]],
        mode="markers",
        marker=dict(color="blue", size=14, symbol="triangle-down-open-dot"),
        name="Minimum snow height",
        showlegend=False,
        legendgroup="HS",
    ), secondary_y=False)
    fig.add_trace(go.Scatter(
        x=[hs_max_row.name],
        y=[hs_max_row["HS"]],
        mode="markers",
        marker=dict(color="blue", size=14, symbol="triangle-up-open-dot"),
        name="Maximum snow height",
        showlegend=False,
        legendgroup="HS",
    ), secondary_y=False)
    ta_min_row = df[df["TA"] == df["TA"].min()].iloc[0]
    ta_max_row = df[df["TA"] == df["TA"].max()].iloc[0]
    fig.add_trace(go.Scatter(
        x=[ta_min_row.name],
        y=[ta_min_row["TA"]],
        mode="markers",
        marker=dict(color="red", size=14, symbol="triangle-down-open-dot"),
        name="Minimum air temperature",
        showlegend=False,
        legendgroup="TA",
    ), secondary_y=True)
    fig.add_trace(go.Scatter(
        x=[ta_max_row.name],
        y=[ta_max_row["TA"]],
        mode="markers",
        marker=dict(color="red", size=14, symbol="triangle-up-open-dot"),
        name="Maximum air temperature",
        showlegend=False,
        legendgroup="TA",
    ), secondary_y=True)

    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=df["HS"],
            name="Snow height (m)",
            line={"color": "blue"},
            legendgroup="HS",
            hovertemplate="HS: %{y:.2f} m<extra>%{x}</extra>",
        ),
        secondary_y=False,
    )
    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=df["TA"],
            name="Air temperature (°C)",
            line={"color": "red"},
            legendgroup="TA",
            opacity=0.9,
            hovertemplate="TA: %{y:.2f} °C<extra>%{x}</extra>",
        ),
        secondary_y=True,
    )
    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=[0] * len(df.index),
            name="0 °C",
            line={"color": _salmonred},
            legendgroup="TA",
            opacity=0.3,
            hovertemplate="TA: 0°C",
        ),
        secondary_y=True,
    )
    fig.update_layout(
        title_text=f'Closest AWS "{stat_name}" ({stat_id}) is {round(stat_dist, 2)} km away:',
    )

    fig.update_xaxes(title_text=None) # we have the date clearly displayed
    fig.update_yaxes(title_text="Snow height (m)", secondary_y=False)
    fig.update_yaxes(title_text="Air temperature (°C)", secondary_y=True)
    return fig

def station_plots(domain: str, poi):
    """Prints all variables contained in snow and wind station datasets."""
    data_files, data_path = _get_snow_wind_stations(domain, poi)
    if len(data_files) > 2:
        figempty = _emptyfig(f"Too many files in {data_path}.")
        return figempty, 0, "", ""
    figs = [] # put snow and wind stations as 2 figs
    winds = True
    for fd in data_files:
        fsmet = os.path.join(data_path, fd)
        smet = SMETParser(fsmet)
        stat_lat = smet.get_header_entry("latitude")
        stat_lon = smet.get_header_entry("longitude")
        stat_name = smet.get_header_entry("station_name")
        stat_id = smet.get_header_entry("station_id")
        latlon = poi["location"].split(",")
        latlon = [float(ll) for ll in latlon]
        stat_dist = distance.distance((stat_lat, stat_lon), latlon).km

        df = smet.df()
        if df is None:
            figs.append(_emptyfig("Error fetching ground station data."))
            continue
        fig = px.line(df, x=df.index, y=df.columns)
        fig.update_yaxes(title_text=None) # too many parameters
        fig.update_xaxes(title_text=None)
        stype = "wind station" if winds else "AWS"
        fig.update_layout(
            title_text=f'Closest {stype} "{stat_name}" ({stat_id}) is {round(stat_dist, 2)} km away:'
        )
        figs.append(fig)
        winds = False
    if len(figs) == 1:
        figempty = _emptyfig("No wind station available.")
        figs.insert(0, figempty)
    return figs

def snow_sums(domain: str, poi: dict, wrf_file: str):
    """AWS and WRF snow height forecast as line plot."""
    fig = go.Figure()

    # --- AWS measurements ---
    data_files, data_path = _get_snow_wind_stations(domain, poi)
    if len(data_files) > 2:
        figempty = _emptyfig(f"Too many files in {data_path}.")
        return figempty

    search = (file for file in data_files if not file.endswith("1.smet"))
    snow_stat_path = next(search, None)
    fsmet = os.path.join(data_path, snow_stat_path)
    smet = SMETParser(fsmet)
    df_aws = smet.df()

    hs_diff = df_aws["HS"].diff()
    times = pd.to_datetime(df_aws.index)
    hs_diff.index = times
    hs_diff = hs_diff.dropna()

    daily_diff_sums = hs_diff.resample("D").sum().to_frame() # Series to frame to insert cols
    daily_diff_sums["color"] = np.where(daily_diff_sums["HS"] < 0, "orangered", _lightblue)

    fig.add_trace(
        go.Bar(
            x=daily_diff_sums.index,
            y=daily_diff_sums["HS"],
            name="Measured",
            marker_color=daily_diff_sums["color"],
            text=[f"{round(hs, 2):+}" for hs in daily_diff_sums["HS"]], # force sign
            textfont=dict(color="gray"),
            textposition="outside",
            showlegend=False,
            legendgroup="measured",
        )
    )
    fig.add_trace( # add invisible trace to force legend color
        go.Scatter( # (otherwise it would be red or blue depending on 1st)
            x=[None],
            y=[None],
            name="Measured",
            mode="lines",
            marker_color=_lightblue,
            legendgroup="measured",
        )
    )

    # --- Model output ---

    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt) for dt in wrf_dates]
    latlon = poi["location"].split(",")
    latlon = (float(latlon[0]), float(latlon[1]))

    param = "SNOWH"
    ts, _, _ = _extract_time_series(wrffile, param, latlon)

    df_mod = pd.DataFrame({"timestamp": wrf_dates, "HS": ts})
    df_mod.set_index("timestamp", inplace=True, drop=True)
    hs_diff_mod = df_mod.diff()
    daily_diff_sums_mod = hs_diff_mod.resample("D").sum()
    daily_diff_sums_mod["color"] = np.where(daily_diff_sums_mod["HS"] < 0, "darkred", _darkblue)
    fig.add_trace(
        go.Bar(
            x=daily_diff_sums_mod.index,
            y=daily_diff_sums_mod["HS"],
            name="Modeled",
            marker_color=daily_diff_sums_mod["color"],
            text=[f"{round(hs, 2):+}" for hs in daily_diff_sums_mod["HS"]],
            textfont=dict(color="gray"),
            textposition="outside",
            showlegend=False,
            legendgroup="modeled",
            visible="legendonly", # to start "modeled" as deselected
        )
    )
    fig.add_trace( # add invisible trace to force legend color
        go.Scatter( # (otherwise it would be red or blue depending on 1st)
            x=[None],
            y=[None],
            name="Modeled",
            mode="lines",
            marker_color=_darkblue,
            legendgroup="modeled",
            visible="legendonly", # to start "modeled" as deselected
        )
    )
    wrffile.close()

    today = datetime.datetime.now()
    fig.update_layout(
        title="Daily difference in snow height from ground measurements and from NWP (WRF):",
        xaxis_title="(double click to view whole season)",
        yaxis_title="Amount (m)",
        xaxis=dict(range=[today - datetime.timedelta(days=7), today]), # focus on last week on startup
        bargap=0.1, # between days
        bargroupgap=0.0, # between meas/mod
    )
    return fig

def clouds_plot(domain: str, poi: dict, wrf_file: str, _timers: bool=False):
    stime = time.time_ns()
    wrf_file = Dataset(wrf_file)
    if _timers: print(f"[T] Open wrf_file: {awio.iso_timediff(stime, True)}")
    wrf_dates = wrf.extract_times(wrf_file, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt) for dt in wrf_dates]

    latlon = poi["location"].split(",")
    latlon = (float(latlon[0]), float(latlon[1]))
    x_y = wrf.ll_to_xy(wrf_file, latlon[0], latlon[1])
    x, y = x_y[0], x_y[1]

    stime = time.time_ns()
    ph = wrf.getvar(wrf_file, "PH", timeidx=0).sel(west_east=x, south_north=y) # perturbation geopotential
    phb = wrf.getvar(wrf_file, "PHB", timeidx=0).sel(west_east=x, south_north=y) # base state geopotential
    elevation_levels = (ph + phb) / 9.81 # elevations the vertical model levels correspond to

    cloud_fraction = wrf.getvar(wrf_file, "CLDFRA", timeidx=wrf.ALL_TIMES)
    cloud_fraction = cloud_fraction.sel(west_east=x, south_north=y)
    if _timers: print(f"[T] Read ph, phb, cldfra: {awio.iso_timediff(stime, True)}")

    stime = time.time_ns()
    trace = go.Contour(
        z=wrf.to_np(cloud_fraction).T,
        x=cloud_fraction.Time,
        y=elevation_levels.values,
        colorscale="greys",
        showlegend=True,
        name="Raw cloud fraction",
        hovertemplate="%{x}<br>Cloud frac: <b>%{z:.3f}</b><extra>Altitude:<br>%{y:.0f} m</extra>",
        colorbar=dict(title="CLDFRA", len=0.5),
    )
    if _timers: print(f"[T] Plot cldfra: {awio.iso_timediff(stime, True)}")

    stime = time.time_ns()
    vert = wrf.getvar(wrf_file, "z", timeidx=wrf.ALL_TIMES)
    if _timers: print(f"[T] Read vert: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    rh = wrf.getvar(wrf_file, "rh", timeidx=wrf.ALL_TIMES)
    if _timers: print(f"[T] Read rh: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    ctt = wrf.getvar(wrf_file, "ctt", timeidx=wrf.ALL_TIMES)
    if _timers: print(f"[T] Read ctt: {awio.iso_timediff(stime, True)}")
    ctt = ctt.sel(west_east=x, south_north=y)
    ctt_np = wrf.to_np(ctt).filled(0)

    cld_height_levels = [500, 2000, 4000] # thresholds are bottom values
    stime = time.time_ns()
    cld_aggregated = wrf.cloudfrac(
        vert,
        rh,
        vert_inc_w_height=True,
        low_thresh=cld_height_levels[0],
        mid_thresh=cld_height_levels[1],
        high_thresh=cld_height_levels[2]
    )
    if _timers: print(f"[T] Cloudfrac interpolation: {awio.iso_timediff(stime, True)}")
    cld_aggregated = cld_aggregated.sel(west_east=x, south_north=y)
    clda_np = wrf.to_np(cld_aggregated)
    # We have nans if the terrain elevation is above the clouds:
    clda_np = clda_np.filled(0) # 0 means no clouds for us

    hover_texts = []
    trace_names = ["Low clouds (500-2000 m)", "Mid clouds (2000-4000 m)", "High clouds (> 4000 m)"]
    for jj in range(len(trace_names)):
        hover_texts.append(["Cloud frac: <b>" + str(round(clda_np[jj][ii], 3)) +
            "</b><br>" + str(round(ctt_np[ii], 2)) + " °C" + "<br>" +
            str(round(ctt_np[ii] + 273.15, 2)) + " K" for ii in range(len(ctt_np))])
    lvl_traces = []
    for lvl in range(len(cld_height_levels)):
        lvl_trace = go.Scatter(
            x=wrf_dates,
            y=[cld_height_levels[lvl]] * len(wrf_dates),
            name=trace_names[lvl],
            text=hover_texts[lvl],
            hoverinfo="x+text+name",
            mode="markers",
            opacity=0.8,
            # size increase decreases approaching 1:
            marker=dict(size=np.sqrt(clda_np[lvl]) * 50, color=ctt_np, colorscale="Jet")
        )
        lvl_traces.append(lvl_trace)

    trace_ctt_colorbar = go.Contour(
        # this trace is just to get a 2nd colorbar for CTT:
        z=[ctt_np.min(), ctt_np.max()],
        x=[wrf_dates[0], wrf_dates[1]],
        y=[0, 0],
        colorscale="Jet",
        colorbar={
            "title": "CTT (°C)",
            "len": 0.5,
            "x": 1.08,
        },
        opacity=0,
    )

    fig = go.Figure(data=[trace] + lvl_traces + [trace_ctt_colorbar])
    fig.update_layout(
        title="Cloud fraction and top of cloud temperature:",
        yaxis_title="Altitude (m)",
        legend={"traceorder": "reversed"},
        yaxis={
            "showgrid": False,
            "range": [0, 12e3], # init with 12 km, but can scroll to unreasonable heights
        },
        xaxis={"range": [wrf_dates[0], wrf_dates[-1]]},
    )
    wrf_file.close()
    return fig

def wind_barbs(domain: str, poi: dict):

    data_files, data_path = _get_snow_wind_stations(domain, poi)
    if len(data_files) != 2:
        figempty = _emptyfig(f"Can not identify wind from data files.")
        return figempty

    search = (file for file in data_files if file.endswith("1.smet"))
    snow_stat_path = next(search, None)
    fsmet = os.path.join(data_path, snow_stat_path)
    smet = SMETParser(fsmet)
    df = smet.df()
    df = df.dropna(subset=["VW", "VW_MAX", "DW"])

    df.loc[:, "VW_MAX"] = df["VW_MAX"] * 3.6 # m/s -> km/h
    df.loc[:, "VW"] = df["VW"] * 3.6
    daily_max_wind = df["VW_MAX"].resample("D").max()

    # merge the max speeds with the original data to align with their respective directions:
    daily_data = df.merge(
        daily_max_wind.rename("daily_max_speed"),
        left_on=df.index.date,
        right_on=daily_max_wind.index.date
    )
    daily_data.rename(columns={"key_0": "timestamp"}, inplace=True)
    # filter multiple measurements from same day:
    daily_data = daily_data[daily_data["VW_MAX"] == daily_data["daily_max_speed"]]

    bins_vw = [0, 20, 40, 60, 100, 250]
    cmap = ["#89CFF0", "#B19CD9", "#7B68EE", "#E56717", "#FF0000"]
    daily_data["color"] = pd.cut(daily_data["VW_MAX"], bins=bins_vw, labels=cmap)
    daily_data = daily_data.dropna(subset=["daily_max_speed", "color"])

    daily_data.drop_duplicates(subset="timestamp", keep="last", inplace=True)

    def _calc_xmin(row):
        dw_rad = np.radians(row["DW"])
        dx = np.sin(dw_rad) * row["DW"] / 2
        _capped_speed = 100 # 100 km/h: 24 h
        dx = dx * 12 / _capped_speed
        # make sure to include time information by calling explicit constructor:
        timestamp = row["timestamp"]
        ts_with_time = datetime.datetime(timestamp.year, timestamp.month, timestamp.day)
        sdate = ts_with_time - datetime.timedelta(hours=dx)
        edate = ts_with_time + datetime.timedelta(hours=dx)
        return sdate

    def _calc_xmax(row): # TODO: functools to unify these 2 functions with additional arg
        dw_rad = np.radians(row["DW"])
        dx = np.sin(dw_rad) * row["DW"] / 2
        _capped_speed = 100 # 100 km/h: 24 h
        dx = dx * 12 / _capped_speed
        # make sure to include time information by calling explicit constructor:
        timestamp = row["timestamp"]
        ts_with_time = datetime.datetime(timestamp.year, timestamp.month, timestamp.day)
        edate = ts_with_time + datetime.timedelta(hours=dx)
        return edate

    def _calc_yspan(row):
        dw_rad = np.radians(row["DW"])
        dy = np.cos(dw_rad) * row["VW_MAX"] / 30
        _capped_speed = 100 # 100 km/h: 24 h
        dy = dy * 12 / _capped_speed
        return dy

    daily_data["sdate"] = daily_data.apply(_calc_xmin, axis=1)
    daily_data["edate"]  = daily_data.apply(_calc_xmax, axis=1)
    daily_data["yy"] = daily_data.apply(_calc_yspan, axis=1)

    fig = make_subplots(specs=[[{"secondary_y": True}]])
    # add invisible markers with hover info:
    hover_texts = [f"Speed: {speed:.2f} km/h, Direction: {direction:.2f}°" for speed, direction in zip(daily_data["daily_max_speed"], daily_data["DW"])]
    fig.add_trace(go.Scatter(
        x=daily_data["timestamp"],
        y=[0] * len(daily_data),
        mode="markers",
        marker=dict(color="rgba(0,0,0,0)"),
        hoverinfo="text",
        text=hover_texts,
        showlegend=False,
    ), secondary_y=False)

    fig.add_trace(go.Bar(
        x=daily_data["timestamp"],
        y=daily_data["VW_MAX"],
        name="Max wind burst",
        marker=dict(color="#428755"),
        opacity=0.8,
    ), secondary_y=True)

    fig.add_trace(go.Scatter(
        x=df.index,
        y=df["VW"],
        name="Wind speed",
        mode="lines",
        line={'shape': 'spline', 'smoothing': 1.3}, # minimal smoothing (1.3 is max val)
        marker=dict(color="#0d3016")
    ), secondary_y=True)

    list_of_all_arrows = []
    for ii in range(len(daily_data)):
        row = daily_data.iloc[ii, :]
        arrow = go.layout.Annotation(dict(
            x=row["edate"],
            y=row["yy"],
            xref="x", yref="y",
            text="",
            showarrow=True,
            axref="x", ayref="y",
            ax=row["sdate"],
            ay=-row["yy"],
            arrowhead=3,
            arrowwidth=5,
            arrowcolor=row["color"],)
        )
        list_of_all_arrows.append(arrow)
    fig.update_layout(annotations=list_of_all_arrows)

    fig.update_yaxes(
        range=[-4, 0],
        visible=False,
        secondary_y=False
    )

    fig.update_layout(
        title="Daily maximum wind bursts with direction and wind speed:",
        xaxis_title="",
        yaxis_title="Max wind burst speed (km/h)",
        bargap=0.0,
    )

    return fig

def solar_plot(domain: str, poi: dict):
    data_files, data_path = _get_snow_wind_stations(domain, poi)
    search = (file for file in data_files if not file.endswith("1.smet"))
    snow_stat_path = next(search, None)
    fsmet = os.path.join(data_path, snow_stat_path)
    smet = SMETParser(fsmet) # read snow station

    df = smet.df()
    if not "ISWR" in df:
        fig = _emptyfig("No solar data available.")
        return fig

    df = df.dropna(subset=["ISWR"])
    # remove time zone info from SMET data to be able to compare:
    daily_mean = df["ISWR"].resample("D").mean()
    iswr_trace = go.Bar(
        x=daily_mean.index,
        y=daily_mean,
        name="ISWR mean",
        opacity=0.5,
        marker=dict(color="#FFD700")
    )
    fig = go.Figure(data=[iswr_trace])
    fig.update_layout(
        title="Daily mean incoming short wave radiation:"
    )
    fig.data[0]["showlegend"] = True
    return fig

def single_raster_chart(domain: str, poi: dict, param: str, wrf_file: str,
        timeidx: int=0, bounds=None, add=None, multiply=None, colorscale=None, title: str=None):
    """WRF raster chart with traces for shape and POI."""

    wrffile = Dataset(wrf_file)
    data = wrf.getvar(wrffile, param, timeidx=timeidx)
    if add:
        data.values = data.values + add
    if multiply:
        data.values = data.values * multiply

    if not bounds:
        bounds = [data.values.min(), data.values.max()]
    if not bounds[1]:
        bounds[1] = math.ceil(data.values.max() * 2) / 2

    # Parameter countour plot:
    fig_cont = go.Contour(
        z=data,
        name="Snow height",
        hovertemplate="HS: %{z:.2f} m<extra>%{x}, %{y}</extra>",
        contours=dict(start=bounds[0], end=bounds[1]), colorscale=colorscale)
    # shape and POI plots:
    fig_poi = _make_poi_fig(domain, poi, wrffile)
    fig_region = _make_region_fig(domain, wrffile)

    fig = go.Figure()
    fig.add_trace(fig_cont)
    fig.add_trace(fig_poi)
    fig.add_trace(fig_region)
    if not title:
        wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
        wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]
        title = "[WRF forecast for " + wrf_dates[timeidx] + ": " + data.description + " (" + data.units + ")"
    fig.update_layout(title=title)
    wrffile.close()
    return fig

def create_param_chart(param: str, domain: str, poi: str, wrf_file: str, ptype="contour",
        bounds=None, add=None, multiply=None, colorscale=None):
    """Plot WRF raster data with time slider."""

    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_stepped = awio.choose_display_dates(wrf_dates)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    fig = go.Figure()
    temp_slider = TimeSlider(fig, domain, poi, wrffile)

    for dt in wrf_stepped:
        idx = dt[1]
        data = wrf.getvar(wrffile, param, timeidx=idx)
        if add:
            data.values = data.values + add
        if multiply:
            data.values = data.values * multiply
        title = "[WRF forecast for " + wrf_dates[idx] + ": " + data.description + " (" + data.units + ")"
        temp_slider.add_time_step(data, ptype=ptype, bounds=bounds, title=title, colorscale=colorscale)

    idx = 0
    for step in wrf_stepped:
        datestr = pd.to_datetime(step[0]).isoformat()
        fig["layout"]["sliders"][0]["steps"][idx]["label"] = datestr
        idx += 1

    wrffile.close()
    return fig

def snowmap_3d(poi: dict, wrf_file: str):
    """3d view of DEM colored by WRF snow water equivalent data."""
    fig = go.Figure()

    param = "SNOW"
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    data = wrf.getvar(wrffile, param, timeidx=len(wrf_dates) - 1)
    terra = wrf.g_terrain.get_terrain(wrffile, timeidx=0, method="cat",
        squeeze=True, cache=None, meta=False, _key=None, units="m")
    terra = pd.DataFrame(np.ma.filled(terra)) # fill masked sparse array

    fig.add_trace(go.Surface(
        z=terra.values.tolist(),
        surfacecolor=data,
        colorscale="blues")
    )
    fig.update_traces(contours_z={ # elevation lines and countours on top & bottom
        "show": True,
        "usecolormap": True,
        "highlightcolor":
        "magenta",
        "project_z": True}
    )
    lat, lon = poi["location"].split(",")
    lat_lon_idx = wrf.ll_to_xy(wrffile, float(lat), float(lon))
    lat = lat_lon_idx.data[0]
    lon = lat_lon_idx.data[1]
    fig.add_trace(go.Scatter3d(
        x=[lat], # middle for centered WRF runs...
        y=[lon],
        z=[terra[lat][lon] + 500]
    ))

    fig.update_layout(
        template="plotly_white",
        title=f"Snow water equivalent on DEM for {wrf_dates[-1]} ({data.units})"
    )
    fig.update_scenes( # 3d scene options
        aspectratio=dict(x=2, y=2, z=1),
        aspectmode="manual"
    )
    wrffile.close()
    return fig

def snow_height_timeseries(poi: dict, wrf_file: str):
    """WRF snow height forecast as line plot."""
    fig = go.Figure()
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    latlon = poi["location"].split(",")
    latlon = (float(latlon[0]), float(latlon[1]))

    params = ["SNOWH"]
    for param in params:
        ts, _, units = _extract_time_series(wrffile, param, latlon)
        caption = f"{param} ({units})"
        fig.add_trace(go.Scatter(x=wrf_dates, y=ts, mode="lines", name=caption))

    fig.update_layout(
        title="Snow Height (WRF forecast)",
        yaxis_title=f"Snow height ({units})",
    )
    wrffile.close()
    return fig

def precip_timeseries(poi: dict, wrf_file: str):
    """WRF parameters related to precipitation."""
    fig = go.Figure()
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    latlon = poi["location"].split(",")
    latlon = (float(latlon[0]), float(latlon[1]))

    params = ["SNOWNC", "GRAUPELNC", "HAILNC", "RAINC", "RAINNC", "RAINSH"]
    annotation = ""
    ts = {}
    for param in params:
        ts[param], description, units = _extract_time_series(wrffile, param, latlon)
        caption = f"{param} ({units})"
        fig.add_trace(go.Scatter(x=wrf_dates, y=ts[param], mode="lines", name=caption))
        annotation += f'**{param}**: {description.capitalize()} _({units})_ '

    # RAINNC: Cumulative, non-convective total precipitation
    # RAINC: Cumulative rainfall from deep convective processes
    # RAINSH: Cumulative rainfall from shallow convective processes
    rain = ts["RAINNC"] - ts["SNOWNC"] - ts["GRAUPELNC"] - ts["HAILNC"] + ts["RAINC"] + ts["RAINSH"]
    fig.add_trace(go.Scatter(x=wrf_dates, y=rain, mode="lines", name=f"Rain ({units})"))
    annotation += f'**Rain**: Liquid precipitation including convective and shallow convective rain _({units})_ '

    annotation = annotation[:-1]
    fig.update_layout(
        title="Precipitation time series extracted from WRF forecast",
        hovermode="x unified",
    )
    wrffile.close()
    return fig, annotation

def param_heatmap_plotlyslider(wrf_file: str, param: str, _timers: bool=False):
    """Animated heatmap with base map.

    This function uses plotly express for the time animation.
    This encapsulates everythin nicely in one plot, but that does include the
    data so if there is a lot then this method is very slow for display.
    param_heatmap() should be used instead; this is kept for quick tries.
    """
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    stime = time.time_ns()
    data = []
    lats = []
    lons = []
    for tt in range(len(wrf_dates)):
        data_tt = wrf.getvar(wrffile, param, timeidx=tt)
        description = data_tt.description # the same each loop...
        units = data_tt.units
        lats_tt, lons_tt = wrf.latlon_coords(data_tt)

        # The data is returned as a list of lists representing the
        # rows on the grid. For the heatmap we need one long list.
        # Or rather three:
        # [lat1, lat2, ...], [lon1, lon2, ...], [T1, T2, ...]
        # We need one such list for every time step (data, lats, lons):
        data_tt = list(chain.from_iterable(data_tt))
        data.append(data_tt)
        lats_tt = wrf.to_np(lats_tt)
        lats_tt = list(chain.from_iterable(lats_tt))
        lats.append(lats_tt)
        lons_tt = wrf.to_np(lons_tt)
        lons_tt = list(chain.from_iterable(lons_tt))
        lons.append(lons_tt)

    # Finally, we create a list of dictionaries with unique indices (from
    # 1, ..., <nr of total data points>) as IDs, and the date/data/lat/lon
    # as values. Data points are added summed according to this index. On
    # our grid we have one measurement per grid point, so each "pixel"
    # receives only one value. From these we build a DataFrame which
    # can be passed to the heatmap generation.
    ddate = {}
    ddata = {}
    dlats = {}
    dlons = {}
    idx = 0
    for tt in range(len(wrf_dates)):
        for ii in range(len(data[tt])):
            idx += 1
            ddate[idx] = wrf_dates[tt]
            ddata[idx] = data[tt][ii]
            dlats[idx] = lats[tt][ii]
            dlons[idx] = lons[tt][ii]
    df = pd.DataFrame({"datetime": ddate, param: ddata, "lat": dlats, "lon": dlons})
    if _timers: print(f"[T] {param} data prep: {awio.iso_timediff(stime, True)}")

    stime = time.time_ns()
    fig = px.density_mapbox( # heat map with MapBox baseamap
        df,
        z=df[param],
        lat=df["lat"],
        lon=df["lon"],
        range_color=(-20, 20),
        animation_frame="datetime",
        hover_name=param,
        radius=10,
        opacity=0.8,
        zoom=5
    )
    if _timers: print(f"[T] Plot {param} heatmap: {awio.iso_timediff(stime, True)}")
    stime = time.time_ns()
    _mapbox_albina(fig) # Albina base tiles
    if _timers: print(f"[T] {param} Mapbox: {awio.iso_timediff(stime, True)}")
    fig.update_layout(title=f"{description} ({units}) [WRF forecast]")
    wrffile.close()
    return fig

def temperature_levels(wrf_file: str, _timers: bool=False):
    """Stacked surface plots of temperature at elevations."""
    param = "tc"
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    tt = wrf.getvar(wrffile, param, timeidx=len(wrf_dates) - 1)
    zz = wrf.getvar(wrffile, "z", timeidx=len(wrf_dates) - 1)
    descr = tt.description.capitalize()
    units = tt.units

    elevation_levels = [500, 1000, 2000, 3000, 4000]
    stime = time.time_ns()
    tt_interpol = wrf.interplevel(tt, zz, elevation_levels)
    if _timers: print(f"[T] T interpol: {awio.iso_timediff(stime, True)}")

    figs = []
    stime = time.time_ns()
    for lvl in range(len(elevation_levels)):
        tt_lvl = tt_interpol[lvl, :, :]
        tt_lvl *= 3 # exaggeration
        tt_lvl += lvl * 50# drawing offset
        figs.append(go.Surface(z=tt_lvl, showscale=False))

    fig = go.Figure(
        data=figs,
    )
    if _timers: print(f"[T] Plot T interpol: {awio.iso_timediff(stime, True)}")

    lvltxt = ", ".join([str(el) for el in elevation_levels])
    fig.update_layout(
        title={"text": f"Temperature at {lvltxt} m"},
    )
    return fig

def temperature_crosssection(domain: str, poi: dict, wrf_file: str):
    param = "tc"
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    latlon = poi["location"].split(",")
    latlon = (float(latlon[0]), float(latlon[1]))
    tt = wrf.getvar(wrffile, param, timeidx=wrf.ALL_TIMES)
    lat_lon_idx = wrf.ll_to_xy(wrffile, latlon[0], latlon[1])

    tt = tt.sel(south_north=lat_lon_idx[1], west_east=lat_lon_idx[0])
    df = tt.to_dataframe().reset_index()
    data = df.pivot(index="bottom_top", columns="Time", values="temp")
    data = pd.DataFrame(data.values)
    fig = px.imshow(
        data,
        origin="lower",
    )
    fig.update_layout(
        title="Temperature cross section at POI:",
    )
    return fig

def param_heatmap(poi: dict, wrf_file: str, param: str, sdate=None, edate=None,
        colormap=None, accumulated: bool=True, single_value=False, zero_trans=False,
        units: str=None, add: float=None, multiply: float=None, differentiate_precip: bool=True):
    hover_bg = _lightblue
    if param == "SNOWNC":
        if not colormap:
            colormap = "Blues"
    elif param == "RAINNC":
        if not colormap:
            colormap = "deep"
        hover_bg = "green"
    elif param == "GRAUPELNC":
        if not colormap:
            colormap = "YlOrRd"
        hover_bg = "orange"
    elif param == "HAILNC":
        if not colormap:
            colormap = "PuRd"
        hover_bg = "purple"
    if not colormap:
        colormap = "Viridis"

    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)

    s_idx = e_idx = None
    if not sdate:
        s_idx = 0
    if not edate:
        e_idx = len(wrf_dates) - 1

    for ii in range(len(wrf_dates)):
        if s_idx is None and wrf_dates[ii] >= sdate:
            s_idx = ii
        if e_idx is None and wrf_dates[ii] >= edate:
            e_idx = ii
            break

    out_param = param
    e_val = wrf.getvar(wrffile, param, timeidx=e_idx)
    if units is None: # not just empty string
        units = e_val.units

    # RAINNC is the total precipitation including solids. So we remove those,
    # and we add the convective and shallow convective part to get total rain:
    def _merge_rain(val, timeidx):
        val = val - wrf.getvar(wrffile, "SNOWNC", timeidx=timeidx)
        val = val - wrf.getvar(wrffile, "GRAUPELNC", timeidx=timeidx)
        val = val - wrf.getvar(wrffile, "HAILNC", timeidx=timeidx)
        val = val + wrf.getvar(wrffile, "RAINC", timeidx=timeidx)
        val = val + wrf.getvar(wrffile, "RAINSH", timeidx=timeidx)
        return val

    if param == "RAINNC" and differentiate_precip:
        e_val = _merge_rain(e_val, e_idx)
        out_param = "RAIN"
    if single_value:
        data = e_val
        plot_title = f"{param} at selected time step:"
    else:
        s_val = wrf.getvar(wrffile, param, timeidx=s_idx)
        if param == "RAINNC" and differentiate_precip:
            s_val = _merge_rain(s_val, s_idx)
        data = (e_val - s_val) # cumsum from start- to end-date
        plot_title = f"Difference of {out_param} in selected time span:"
    max_s_n, max_w_e = np.unravel_index(data.argmax(), data.shape) # rolling index
    max_lat = data["XLAT"].values[max_s_n, max_w_e]
    max_lon = data["XLONG"].values[max_s_n, max_w_e]

    if add:
        data = data + add
    if multiply:
        data = data * multiply
    data = data.to_dataframe(name=param) # name is lost in _merge_rain()
    max_val = data[param].max().max() # first column- then row-wise
    if accumulated: # use if data can not go below 0
        min_val = 0 # --> better color regions in this case
    else:
        min_val = data[param].min().min()
    data["param_name"] = out_param + ":" # for custom data
    data["param_unit"] = units
    lats, lons = wrf.latlon_coords(e_val)
    lats = list(chain.from_iterable(wrf.to_np(lats)))
    lons = list(chain.from_iterable(wrf.to_np(lons)))

    if min_val == 0 and max_val == 0:
        # If there is no data to display the heatmap would still sum
        # up to the highest color, for whatever reason. In this case
        # set a completely transparant colormap:
        colormap = ["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0)"]
    else:
        if zero_trans:
            high = max(abs(min_val), abs(max_val))
            min_val = high * -1
            max_val = high
    fig = px.density_mapbox( # plot data
        data,
        z=param,
        lat=lats,
        lon=lons,
        radius=20,
        range_color=(min_val, max_val),
        labels={"z": param},
        custom_data=["param_name", "param_unit"],
        opacity=0.9,
        color_continuous_scale=colormap
    )
    fig.update_traces(
        hovertemplate=_get_param_heatmap_hover_templ(),
        hoverlabel={"bgcolor": hover_bg},
    )
    if min_val != 0 or max_val != 0:
        fig.add_trace(go.Scattermapbox( # plot extreme values
            lat=[max_lat],
            lon=[max_lon],
            mode="markers",
            customdata=[f"Max {param}<br><b>{max_val:.1f} {units}</b>"], # left side of hover box
            hovertemplate=_get_maxval_hover_templ(),
            showlegend=False,
            marker=go.scattermapbox.Marker(
                size=15,
                color="cyan",
                symbol="circle",
                opacity=0.7,
            ),
            name=f"Max {param}",
        ))
    poi_lat, poi_lon = poi["location"].split(", ")
    fig.add_trace(go.Scattermapbox( # plot POI
        lat=[poi_lat],
        lon=[poi_lon],
        mode="markers",
        hovertemplate="POI " + poi["locationName"],
        showlegend=False,
        marker=go.scattermapbox.Marker(
            size=15,
            color="red",
            symbol="circle",
            opacity=0.7,
        ),
        name=f"POI {poi['locationName']}",
    ))
    fig.update_layout(title=plot_title)
    if accumulated:
        fig.update_layout(
            # Remove the colorbar ticks as they do not make much
            # sense for the whole region - an annoyance of not
            # being able to draw contour plots:
            coloraxis_colorbar={"tickvals": []}
        )
    _mapbox_albina(fig)

    wrffile.close()
    return fig

def windmap_uvwz(wrf_file: str):
    """Plot that allows to switch between the different wind components."""
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]
    u10 = wrf.getvar(wrffile, "U10", timeidx=len(wrf_dates) - 1)
    v10 = wrf.getvar(wrffile, "V10", timeidx=len(wrf_dates) - 1)
    z10 = wrf.getvar(wrffile, "W", timeidx=len(wrf_dates) - 1)
    z10 = _4d_select(z10, verbose=False)
    wspd10 = np.sqrt(np.square(u10) + np.square(v10)) # wind speed

    lbound = float( min([u10.min(), v10.min(), z10.min()]) ) # min of all - keep scale
    ubound = float( max([u10.max(), v10.max(), z10.max()]) )
    cont_comp=dict(
        start=lbound,
        end=ubound,
        size=1
    )
    cont_wspd = go.Contour(z=wspd10, name="Wspd", colorscale="PuRd")
    cont_uu = go.Contour(z=u10, name="U10", visible=False, colorscale="RdBu", contours=cont_comp)
    cont_vv = go.Contour(z=v10, name="V10", visible=False, colorscale="RdBu", contours=cont_comp)
    cont_zz = go.Contour(z=z10, name="Z10", visible=False, colorscale="RdBu", contours=cont_comp)

    layout = go.Layout(
        updatemenus=[
            dict(
                type="buttons",
                direction="up",
                buttons=[
                    dict(
                        label="Wdsp",
                        method="update",
                        args=[{"visible": [True, False, False, False]},
                            {"title": f"Wind speed at 10 m above ground ({u10.units})"}]
                    ),
                    dict(
                        label="U",
                        method="update",
                        args=[{"visible": [False, True, False, False]},
                            {"title": f"U wind component (E-W, positive is W, {u10.units})"}]
                    ),
                    dict(
                        label="V",
                        method="update",
                        args=[{"visible": [False, False, True, False]},
                            {"title": f"V wind component (N-S, positive is S, {u10.units})"}]
                    ),
                    dict(
                        label="Z",
                        method="update",
                        args=[{"visible": [False, False, False, True]},
                            {"title": f"Z wind component (bottom-top, pos. is upward, {u10.units})"}]
                    ),
                ],
            )
        ]
    )

    fig = go.Figure(
        data=[cont_wspd, cont_uu, cont_vv, cont_zz],
        layout=layout
    )
    fig.update_layout(title=f"Wind speed at 10 m above ground ({u10.units})")
    wrffile.close()
    return fig

def albedo_map_plotlyslider(domain: str, poi, wrf_file: str):
    """Multiple figures sharing the same colorscale.

    This function uses plotly-internal sliders, meaning all data
    will be loaded at once, making it mostly useful for low density data.
    """
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    fig = make_subplots(rows=2, cols=2, shared_xaxes="all", shared_yaxes="all",
        subplot_titles=("Albedo", "Background Albedo", "Annual max snow albedo in fraction", "Landuse index"))
    temp_slider = TimeSlider(fig, domain, poi, wrffile)

    for time_step in range(wrffile.dimensions["Time"].size):
        albedo_bkg = wrf.getvar(wrffile, "ALBEDO", timeidx=time_step)
        albedo = wrf.getvar(wrffile, "ALBBCK", timeidx=time_step)
        annual_albedo = wrf.getvar(wrffile, "SNOALB", timeidx=time_step)
        landuse = wrf.getvar(wrffile, "LU_INDEX", timeidx=time_step)
        temp_slider.add_time_step(albedo, bounds=(0, 1), colorscale="inferno", row=1, col=1)
        temp_slider.add_time_step(albedo_bkg, bounds=(0, 1), colorscale="inferno", row=1, col=2)
        temp_slider.add_time_step(annual_albedo, bounds=(0, 1), colorscale="inferno", row=2, col=1)
        temp_slider.add_time_step(landuse, showscale=False, row=2, col=2)

    for step in range(len(wrf_dates)):
        datestr = pd.to_datetime(wrf_dates[step]).isoformat()
        fig["layout"]["sliders"][0]["steps"][step]["label"] = datestr
    wrffile.close()
    return fig

def subplots_map(poi: dict, wrf_file: str, params: list, time_step: int, bounds: list,
        titles: list):
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_stepped = awio.choose_display_dates(wrf_dates)

    fig = make_subplots(rows=2, cols=2, shared_xaxes="all", shared_yaxes="all",
        subplot_titles=titles)

    if len(bounds) == 2: # same bounds for all params
        bounds = [bounds] * 4

    traces = []
    for ii in range(len(params)):
        param_data = wrf.getvar(wrffile, params[ii], timeidx=wrf_stepped[time_step][1])
        traces.append(go.Contour(
            z=param_data,
            contours=dict(start=bounds[ii][0], end=bounds[ii][1]),
            colorscale="inferno",
            line_smoothing=0.5,
            showscale=(ii == 1),
        ))

    for ii in range(4):
        row, col = np.unravel_index(ii, (2, 2)) # flat index to row/col
        fig.add_trace(traces[ii], row=row + 1, col=col + 1)
    wrffile.close()
    return fig

def zero_deg_line(poi, wrf_file: str, domain: str, _timers: bool=False):
    """Interpolate elevation to height of 0 degC."""
    param="tc"
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]

    time_step = len(wrf_dates) - 1 # last time step
    t2 = wrf.getvar(wrffile, param, timeidx=time_step)
    zz = wrf.getvar(wrffile, "z", units="m", timeidx=time_step) # height over terrain
    stime = time.time_ns()
    h0_interpol = wrf.interplevel(zz, t2, 0)
    if _timers: print(f"[T] H0 interpol: {awio.iso_timediff(stime, True)}")

    bounds = (np.nanmin(h0_interpol), np.nanmax(h0_interpol))
    stime = time.time_ns()
    fig_cont = go.Contour(z=h0_interpol, visible=True,
        contours=dict(start=bounds[0], end=bounds[1], size=100), colorscale="viridis")
    if _timers: print(f"[T] Plot H0 interpol: {awio.iso_timediff(stime, True)}")
    fig_poi = _make_poi_fig(domain, poi, wrffile)
    fig_region = _make_region_fig(domain, wrffile)

    fig = go.Figure()
    fig.add_trace(fig_cont)
    fig.add_trace(fig_poi)
    fig.add_trace(fig_region)
    title = "Zero degree line (m) [WRF on " + wrf_dates[time_step] + "]"
    fig.update_layout(title=title)
    return fig

def snow_temp_plot(poi: dict, wrf_file: str, request_date=None, colorscale="Blackbody_r", normalize_colors=False):
    """Plot surface skin-, snow layer- and soil layer-temperatures."""
    #TSNO, ZSNSO, SNICE, SNLIQ
    #https://github.com/NCAR/noahmp/blob/981d4f859ce6c64213d38a783654c05b47b3485e/src/module_sf_noahmplsm.F#L5251
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt) for dt in wrf_dates]
    if request_date:
        timeidx = awio.datetools.closest_index(wrf_dates, request_date)
    else:
        timeidx = wrf.ALL_TIMES

    latlon = poi["location"].split(",")
    latlon = (float(latlon[0]), float(latlon[1]))
    lat_lon_idx = wrf.ll_to_xy(wrffile, latlon[0], latlon[1])
    # snow layer temperatures:
    tsno = wrf.getvar(wrffile, "TSNO", timeidx)
    tsno = tsno.sel(south_north=lat_lon_idx[1], west_east=lat_lon_idx[0])
    tsno.values[tsno.values == 0] = np.nan
    tsno.values = tsno.values - 273.15
    # snow and soil layer depths (bottom of layers):
    zsnso = wrf.getvar(wrffile, "ZSNSO", timeidx=timeidx)
    zsnso = zsnso.sel(south_north=lat_lon_idx[1], west_east=lat_lon_idx[0])
    n_snowlayers = len(tsno["snow_layers_stag"].values)
    n_soillayers = len(zsnso["snso_layers_stag"].values) - n_snowlayers
    layer_locs_snow = zsnso.isel(snso_layers_stag=slice(0, n_snowlayers))
    layer_locs_snow.values[layer_locs_snow.values == 0] = np.nan
    layer_locs_soil = zsnso.isel(snso_layers_stag=slice(n_snowlayers, None))
    layer_locs_soil.values[layer_locs_soil.values == 0] = np.nan
    # soil temperatures:
    tslb = wrf.getvar(wrffile, "TSLB", timeidx=timeidx)
    tslb = tslb.sel(south_north=lat_lon_idx[1], west_east=lat_lon_idx[0])
    tslb.values = tslb.values - 273.15
    # surface skin temperature:
    tsk = wrf.getvar(wrffile, "TSK", timeidx=timeidx)
    tsk = tsk.sel(south_north=lat_lon_idx[1], west_east=lat_lon_idx[0])
    tsk.values = tsk.values - 273.15


    # find maximum in all loaded parameters for colorscale:
    if normalize_colors:
        max_abs_val = np.max([np.abs(tsno).max(), np.abs(tslb).max(), np.abs(tsk).max()])
        cmin = -max_abs_val
        cmax = max_abs_val
    else:
        cmin = cmax = None

    traces = []
    tsk_trace = go.Scatter(
        x=wrf_dates,
        y=[0] * len(wrf_dates),
        mode="markers",
        hovertemplate="<b>Surface</b><br>%{x}<extra>%{marker.color:.3f} °C</extra>",
        marker=dict(
            color=tsk.values,
            colorscale=colorscale,
            cmin=cmin,
            cmax=cmax,
            size=10,
        ),
        legendgroup="surface",
        showlegend=False,
    )
    traces.append(tsk_trace)
    for idx in range(len(wrf_dates)):
        sn_trace = go.Scatter(
            x=[wrf_dates[idx]] * n_snowlayers,
            y=layer_locs_snow.values[idx, :],
            marker=dict(
                color=tsno.values[idx, :],
                colorscale=colorscale,
                cmin=cmin,
                cmax=cmax,
                size=12
            ),
            hovertemplate="<b>Snow</b><br>%{x}<br>%{y:.2f} m from surface<extra>%{marker.color:.3f} °C</extra>",
            line=dict(color="blue"),
            legendgroup="snow",
            showlegend=False,
        )
        so_trace = go.Scatter(
            x=[wrf_dates[idx]] * n_soillayers,
            y=layer_locs_soil.values[idx, :],
            hovertemplate="<b>Soil</b><br>%{x}<br>%{y:.2f} m from surface<extra>%{marker.color:.3f} °C</extra>",
            marker=dict(
                color=tslb.values[idx, :],
                colorscale=colorscale,
                cmin=cmin,
                cmax=cmax,
                size=10,
            ),
            line=dict(color="green"),
            opacity=0.8,
            legendgroup="soil",
            showlegend=False,
        )
        traces.append(sn_trace)
        traces.append(so_trace)
    fig = go.Figure(data=traces)

    # forced colors for legend entries (one per type):
    fig.add_trace(
        go.Scatter(
            x=[None],
            y=[None],
            name="Surface",
            marker_color="pink",
            legendgroup="surface",
        )
    )
    fig.add_trace(
        go.Scatter(
            x=[None],
            y=[None],
            name="Snow",
            marker_color=_lightblue,
            legendgroup="snow",
        )
    )
    fig.add_trace(
        go.Scatter(
            x=[None],
            y=[None],
            name="Soil",
            marker_color="green",
            legendgroup="soil",
        )
    )

    fig.update_layout(dict(
        title="Surface, snow and soil temperatures:",
        yaxis_title="Depth (m)",
    ))
    return fig

def nwp_plot(poi: dict, wrf_file: str, param_set="standard"):
    wrffile = Dataset(wrf_file)
    wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    wrf_dates = [pd.to_datetime(dt).isoformat() for dt in wrf_dates]
    df = pd.DataFrame(index=wrf_dates)
    df.index.name = "timestamp"
    lat, lon = poi["location"].split(",")
    lat_lon_idx = wrf.ll_to_xy(wrffile, float(lat), float(lon))
    lat = lat_lon_idx.data[0]
    lon = lat_lon_idx.data[1]
    if param_set == "all":
        data_to_plot = []
        with xr.open_dataset(wrf_file) as ds:
            for var in ds.data_vars:
                if len(ds[var].dims) < 3:
                    continue
                if len(ds[var].dims) > 4:
                    continue
                if "snow_layers_stag" in ds[var].dims:
                    continue
                if "snso_layers_stag" in ds[var].dims:
                    continue
                plot_data = _4d_select(ds[var], verbose=False)

                if plot_data.shape[0] != len(df.index):
                    continue
                plot_data = plot_data[:, lat_lon_idx[1], lat_lon_idx[0]]
                data_to_plot.append(pd.DataFrame(plot_data, columns=[var]))
        df = pd.concat(data_to_plot)
        df = df.astype(float)
        fig = px.line(df, x=df.index, y=df.columns)
    else:
        print(f'[E] Parameter set "{param_set}" unknown to NWP plot.')

    wrffile.close()
    return fig

###############################################
#               SHAPES AND POIS               #
###############################################

def _make_region_fig(domain: str, wrffile):
    lats, lons = _get_domain_latlons(domain)
    xx, yy = _get_wrf_xy(lats, lons, wrffile)
    fig = go.Scatter(
        x=xx,
        y=yy,
        hovertemplate=f"<b>Domain</b><br>{domain.capitalize()}<extra></extra>",
        line=dict(color="black"),
        showlegend=False
    )
    return fig

def _make_poi_fig(domain: str, poi, wrffile):
    lat, lon = poi["location"].split(",")
    lat_lon_idx = wrf.ll_to_xy(wrffile, float(lat), float(lon))
    lat = lat_lon_idx.data[0]
    lon = lat_lon_idx.data[1]

    fig = go.Scatter(
        x=[lat],
        y=[lon],
        hovertemplate=f"<b>POI</b><br>{poi['locationName']}<extra></extra>",
        marker=go.scatter.Marker(
            size=30,
            color="black",
            symbol="arrow-down"
        ),
        showlegend=False
    )
    return fig

################################################
#               HELPER FUNCTIONS               #
################################################

def _mapbox_albina(fig):
    fig.update_layout(
        mapbox_style="white-bg",
        mapbox_layers=[{
            "below": "traces",
            "sourcetype": "raster",
            "sourceattribution": "Albina",
            "source": ["https://static.avalanche.report/tms/{z}/{x}/{y}.png"]
        }]
    )

def _wrf_dates(wrf_file: str, _timers: bool=False):
    stime = time.time_ns()
    with Dataset(wrf_file) as wrffile:
        wrf_dates = wrf.extract_times(wrffile, timeidx=wrf.ALL_TIMES)
    if _timers: print(f"[T] Extract WRF dates: {awio.iso_timediff(stime, True)}")
    return wrf_dates

def _get_station_dist(smet: SMETParser, poi: dict):
    stat_name = smet.get_header_entry("station_name")
    stat_id = smet.get_header_entry("station_id")
    stat_lat = smet.get_header_entry("latitude")
    stat_lon = smet.get_header_entry("longitude")
    latlon = poi["location"].split(",")
    latlon = [float(ll) for ll in latlon]
    stat_dist = distance.distance((stat_lat, stat_lon), latlon).km
    return stat_id, stat_name, stat_dist

def _extract_time_series(wrffile, param, location: tuple):
    lat = location[0]
    lon = location[1]
    data = wrf.getvar(wrffile, param, timeidx=wrf.ALL_TIMES)
    lat_lon_idx = wrf.ll_to_xy(wrffile, lat, lon)
    time_series = data[:, lat_lon_idx[1], lat_lon_idx[0]].values
    return time_series, data.description, data.units

def _get_domain_latlons(domain: str):
    # ST: State
    # BL: Bundesland
    # STAND: datatake date
    shapefile = awset.get(["domain", "shape", "file"], domain)
    shape = fiona.open(shapefile)
    for sp in shape:
        bundesl = sp["properties"]["BL"]
        if bundesl != "Tirol":
            continue
        coords = sp["geometry"]["coordinates"]
        coords = coords[2][0] # pick from MULTIPOLYGON
        lons, lats = list(map(list, zip(*coords)))

    return (lats, lons)

def _get_wrf_xy(lats, lons, wrffile):
    lats = [float(lat) for lat in lats]
    lons = [float(lon) for lon in lons]
    xy = wrf.ll_to_xy(wrffile, lats, lons)
    return xy

def _emptyfig(text: str):
    figempty = px.scatter().add_annotation(text=text,
        showarrow=False, font={"size": 20})
    return figempty

def _get_windrose_hover_templ():
    hover_templ = """
%{fullData.name}<br>
Direction: %{theta}<br>
<extra>
Frequency: %{r:.1f} %<br>
</extra>
    """
    return hover_templ

def _get_param_heatmap_hover_templ():
    hover_templ = """
%{customdata[0]}<br>
<b>%{z:.2f} %{customdata[1]}<b>
<extra>
Position:<br>
(%{lat:.4f}°, %{lon:.4f}°)
</extra>
    """
    return hover_templ

def _get_maxval_hover_templ():
    hover_templ = """
%{customdata}
<extra>
Position:<br>
(%{lat:.4f}°, %{lon:.4f}°)
</extra>
    """
    return hover_templ

def _get_snow_wind_stations(domain: str, poi: dict) -> list:
    data_path = os.path.join(awio.get_scriptpath(__file__),
        "station_data", f"{domain.lower()}-{poi['locationName'].lower()}")
    data_files = os.listdir(data_path)
    data_files.sort() # wind station before weather station
    return data_files, data_path

